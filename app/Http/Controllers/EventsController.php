<?php

namespace App\Http\Controllers;

use App\Events;
use Illuminate\Http\Request;

class EventsController extends Controller
{
    public function index() {
        $events = Events::all();

        return view('events', compact('events'));
    }

    public function store(Request $req) {

        $req->validate([
            'name' => 'required',
            'type' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zip' => 'required',
            'contact_name' => 'required',
            'contact_email' => 'required|email',
            'contact_phone' => 'required|numeric'
        ]);

        $event = new Events([
            'name' => $req->input('name'),
            'type' => $req->input('type'),
            'start_date' => $req->input('start_date'),
            'end_date' => $req->input('end_date'),
            'address' => $req->input('address'),
            'city' => $req->input('city'),
            'state' => $req->input('state'),
            'zip' => $req->input('zip'),
            'contact_name' => $req->input('contact_name'),
            'contact_email' => $req->input('contact_email'),
            'contact_phone' => $req->input('contact_phone'),
        ]);
        $event->save();

        return redirect('/')->with('success', 'Stock has been added');
    }

    public function create() {
        return view('add_event');
    }
}
