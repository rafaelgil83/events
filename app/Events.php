<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
    //
    protected $fillable = [
        'name',
        'type',
        'start_date',
        'end_date',
        'address',
        'city',
        'state',
        'zip',
        'contact_name',
        'contact_email',
        'contact_phone',
    ];
}
