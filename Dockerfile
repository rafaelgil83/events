FROM php:7.1-apache

ENV APACHE_DOCUMENT_ROOT /var/www/html/public

COPY ./develop /etc/apache2/sites-enabled/000-default.conf

RUN mv $PHP_INI_DIR/php.ini-development $PHP_INI_DIR/php.ini

RUN docker-php-ext-install pdo_mysql bcmath && \
    pecl install xdebug-2.6.0 && \
    docker-php-ext-enable xdebug && \
    a2enmod rewrite && \
    echo 'xdebug.remote_port=9000' >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini && \
    echo 'xdebug.remote_enable=1' >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini && \
    echo 'xdebug.remote_autostart=1' >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini && \
    echo "xdebug.remote_log=/var/log/apache2/error.log" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini && \
    echo "xdebug.remote_host=host.docker.internal" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

RUN apt-get update && apt-get install nano sudo && \
    echo 'alias ll="ls -lah"' >> ~/.bashrc
