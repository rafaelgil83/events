@extends('layouts/main')
@section('content')

    <form method="post" action="save">
        <fieldset>
            <legend>General Information</legend>
            <div class="form-group">
                @csrf
                <label for="name">Name:</label>
                <input type="text" class="form-control" name="name" required/>
            </div>
            <div class="form-group">
                <label for="name">Event Type:</label>
                <select name="type" class="form-control w-auto">
                    <option>Small</option>
                    <option>Medium</option>
                    <option>Big</option>
                </select>
            </div>
            <div class="form-group">
                <label for="start_date">Start Date:</label>
                <input class="datepicker" required name="start_date" type="text" readonly>

                <label for="end_date">End Date:</label>
                <input class="datepicker" required name="end_date" readonly>

            </div>
            <div class="form-group">
                <label for="address">Address</label>
                <input class="form-control" name="address" required>

                <label for="city">City</label>
                <input class="form-control" name="city" required>

                <label for="state">State</label>
                <select class="form-control w-auto" name="state" required>
                    <option value="">Select State</option>
                    <option value="AL">Alabama</option>
                    <option value="AK">Alaska</option>
                    <option value="AZ">Arizona</option>
                    <option value="AR">Arkansas</option>
                    <option value="CA">California</option>
                    <option value="CO">Colorado</option>
                    <option value="CT">Connecticut</option>
                    <option value="DE">Delaware</option>
                    <option value="DC">District Of Columbia</option>
                    <option value="FL">Florida</option>
                    <option value="GA">Georgia</option>
                    <option value="HI">Hawaii</option>
                    <option value="ID">Idaho</option>
                    <option value="IL">Illinois</option>
                    <option value="IN">Indiana</option>
                    <option value="IA">Iowa</option>
                    <option value="KS">Kansas</option>
                    <option value="KY">Kentucky</option>
                    <option value="LA">Louisiana</option>
                    <option value="ME">Maine</option>
                    <option value="MD">Maryland</option>
                    <option value="MA">Massachusetts</option>
                    <option value="MI">Michigan</option>
                    <option value="MN">Minnesota</option>
                    <option value="MS">Mississippi</option>
                    <option value="MO">Missouri</option>
                    <option value="MT">Montana</option>
                    <option value="NE">Nebraska</option>
                    <option value="NV">Nevada</option>
                    <option value="NH">New Hampshire</option>
                    <option value="NJ">New Jersey</option>
                    <option value="NM">New Mexico</option>
                    <option value="NY">New York</option>
                    <option value="NC">North Carolina</option>
                    <option value="ND">North Dakota</option>
                    <option value="OH">Ohio</option>
                    <option value="OK">Oklahoma</option>
                    <option value="OR">Oregon</option>
                    <option value="PA">Pennsylvania</option>
                    <option value="RI">Rhode Island</option>
                    <option value="SC">South Carolina</option>
                    <option value="SD">South Dakota</option>
                    <option value="TN">Tennessee</option>
                    <option value="TX">Texas</option>
                    <option value="UT">Utah</option>
                    <option value="VT">Vermont</option>
                    <option value="VA">Virginia</option>
                    <option value="WA">Washington</option>
                    <option value="WV">West Virginia</option>
                    <option value="WI">Wisconsin</option>
                    <option value="WY">Wyoming</option>
                </select>

                <label for="zip">Zip</label>
                <input class="form-control w-25" name="zip" required>
            </div>
            <div class="form-group">
                <label for="contact_name">Contact Name</label>
                <input class="form-control" name="contact_name" type="text" required>

                <label for="contact_email">Contact Email</label>
                <input class="form-control" name="contact_email" type="email" required>
                <label for="contact_phone">Phone</label>
                <input class="form-control" name="contact_phone" type="tel" required>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>

        </fieldset>
    </form>
    <script src="/js/app.js"></script>
@endsection
